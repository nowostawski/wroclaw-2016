import { Component } from 'angular2/core';
import { AfterViewInit } from 'angular2/core';
import { Address } from './Address';

declare var componentHandler: any;

@Component({

  // Declare the tag name in index.html to where the component attaches
  selector: 'gang-of-four-task',

  template: `

  <div class="frame">
    <div class="frame-inner demo-card-wide mdl-card mdl-shadow--2dp">

      <div class="text-normal">
        <h4>Welcome to your private wallet!</h4>
        <h5>Insert new hash:</h5>
      </div>

      <div class="horizontal-normal">

        <div>
          <form action="#">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input class="mdl-textfield__input" [(ngModel)]="newIpfsHash" type="text" id="sample3">
              <label class="mdl-textfield__label" for="sample3">Enter new hash</label>
            </div>
          </form>

          <form action="#">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input class="mdl-textfield__input" [(ngModel)]="newHumanReadableName" type="text" id="sample3">
              <label class="mdl-textfield__label" for="sample3">Enter human readable name for hash</label>
            </div>
          </form>
        </div>

        <div>
          <div class="text-normal">
            <h4>Choose type:</h4>
          </div>

          <div class="horizontal-normal">
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
              <input type="radio" id="option-1" class="mdl-radio__button" name="options" (click)="onBitCoinTypeSelected" checked>
              <span class="mdl-radio__label">BitCoin</span>
            </label>

            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
              <input type="radio" id="option-2" class="mdl-radio__button" name="options" (click)="onEtherTypeSelected">
              <span class="mdl-radio__label">Ether</span>
            </label>
          </div>
        </div>

        <div class="button-normal">
          <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"  (click)="onSave()">
              All right, save it !
          </button>
        </div>

      </div>
    </div>
  </div>

  <div class="frame-list">
    <div class="frame-inner demo-card-wide mdl-card mdl-shadow--2dp">

      <li *ngFor="#item of addressesList">
        Type: {{item.type}}, Human readable name: {{item.humanReadableName}}, IPFS hash: {{item.ipfsHash}}
        -----------------------------------------------------------
      </li>

    </div>
  </div>

`
})

export class GangOfFourTask6 implements AfterViewInit {

  addressesList: Array<Address>;

  newHumanReadableName: string;
  newIpfsHash: string;
  newType: string;
  storageKey = 'ClientSideStorage';

  constructor () {
    this.newType = 'BitCoin';
    this.addressesList = JSON.parse(localStorage.getItem(this.storageKey));
    if (this.addressesList === null) {
      this.addressesList = new Array<Address>();
    }
  }

  ngAfterViewInit() {
      componentHandler.upgradeDom();
  }

  onBitCoinTypeSelected() {
    this.newType = 'BitCoin';
  }

  onEtherTypeSelected() {
    this.newType = 'Ether';
  }

  onSave() {
    var address = new Address();
    address.humanReadableName = this.newHumanReadableName;
    address.ipfsHash = this.newIpfsHash;
    address.type = this.newType;

    console.log('TEST: ' + address.type);
    console.log('TEST: ' + address.humanReadableName);
    console.log('TEST: ' + address.ipfsHash);

    this.addressesList.push(address);
    var addressList = JSON.stringify(this.addressesList);
    localStorage.setItem(this.storageKey, addressList);
  }
}
