System.register(['angular2/core', './Address'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, Address_1;
    var GangOfFourTask6;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (Address_1_1) {
                Address_1 = Address_1_1;
            }],
        execute: function() {
            GangOfFourTask6 = (function () {
                function GangOfFourTask6() {
                    this.storageKey = 'ClientSideStorage';
                    this.newType = 'BitCoin';
                    this.addressesList = JSON.parse(localStorage.getItem(this.storageKey));
                    if (this.addressesList === null) {
                        this.addressesList = new Array();
                    }
                }
                GangOfFourTask6.prototype.ngAfterViewInit = function () {
                    componentHandler.upgradeDom();
                };
                GangOfFourTask6.prototype.onBitCoinTypeSelected = function () {
                    this.newType = 'BitCoin';
                };
                GangOfFourTask6.prototype.onEtherTypeSelected = function () {
                    this.newType = 'Ether';
                };
                GangOfFourTask6.prototype.onSave = function () {
                    var address = new Address_1.Address();
                    address.humanReadableName = this.newHumanReadableName;
                    address.ipfsHash = this.newIpfsHash;
                    address.type = this.newType;
                    console.log('TEST: ' + address.type);
                    console.log('TEST: ' + address.humanReadableName);
                    console.log('TEST: ' + address.ipfsHash);
                    this.addressesList.push(address);
                    var addressList = JSON.stringify(this.addressesList);
                    localStorage.setItem(this.storageKey, addressList);
                };
                GangOfFourTask6 = __decorate([
                    core_1.Component({
                        selector: 'gang-of-four-task',
                        template: "\n\n  <div class=\"frame\">\n    <div class=\"frame-inner demo-card-wide mdl-card mdl-shadow--2dp\">\n\n      <div class=\"text-normal\">\n        <h4>Welcome to your private wallet!</h4>\n        <h5>Insert new hash:</h5>\n      </div>\n\n      <div class=\"horizontal-normal\">\n\n        <div>\n          <form action=\"#\">\n            <div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n              <input class=\"mdl-textfield__input\" [(ngModel)]=\"newIpfsHash\" type=\"text\" id=\"sample3\">\n              <label class=\"mdl-textfield__label\" for=\"sample3\">Enter new hash</label>\n            </div>\n          </form>\n\n          <form action=\"#\">\n            <div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n              <input class=\"mdl-textfield__input\" [(ngModel)]=\"newHumanReadableName\" type=\"text\" id=\"sample3\">\n              <label class=\"mdl-textfield__label\" for=\"sample3\">Enter human readable name for hash</label>\n            </div>\n          </form>\n        </div>\n\n        <div>\n          <div class=\"text-normal\">\n            <h4>Choose type:</h4>\n          </div>\n\n          <div class=\"horizontal-normal\">\n            <label class=\"mdl-radio mdl-js-radio mdl-js-ripple-effect\" for=\"option-1\">\n              <input type=\"radio\" id=\"option-1\" class=\"mdl-radio__button\" name=\"options\" (click)=\"onBitCoinTypeSelected\" checked>\n              <span class=\"mdl-radio__label\">BitCoin</span>\n            </label>\n\n            <label class=\"mdl-radio mdl-js-radio mdl-js-ripple-effect\" for=\"option-2\">\n              <input type=\"radio\" id=\"option-2\" class=\"mdl-radio__button\" name=\"options\" (click)=\"onEtherTypeSelected\">\n              <span class=\"mdl-radio__label\">Ether</span>\n            </label>\n          </div>\n        </div>\n\n        <div class=\"button-normal\">\n          <button class=\"mdl-button mdl-js-button mdl-button--raised mdl-button--colored\"  (click)=\"onSave()\">\n              All right, save it !\n          </button>\n        </div>\n\n      </div>\n    </div>\n  </div>\n\n  <div class=\"frame-list\">\n    <div class=\"frame-inner demo-card-wide mdl-card mdl-shadow--2dp\">\n\n      <li *ngFor=\"#item of addressesList\">\n        Type: {{item.type}}, Human readable name: {{item.humanReadableName}}, IPFS hash: {{item.ipfsHash}}\n        -----------------------------------------------------------\n      </li>\n\n    </div>\n  </div>\n\n"
                    }), 
                    __metadata('design:paramtypes', [])
                ], GangOfFourTask6);
                return GangOfFourTask6;
            }());
            exports_1("GangOfFourTask6", GangOfFourTask6);
        }
    }
});
